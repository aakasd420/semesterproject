json.array!(@dealerships) do |dealership|
  json.extract! dealership, :id, :total_gross_revenue, :net_profit, :sales_tax_totals
  json.url dealership_url(dealership, format: :json)
end
