json.array!(@finances) do |finance|
  json.extract! finance, :id, :simple_annual_interest, :monthly_payments, :amortized
  json.url finance_url(finance, format: :json)
end
