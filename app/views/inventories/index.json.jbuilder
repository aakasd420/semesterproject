json.array!(@inventories) do |inventory|
  json.extract! inventory, :id, :year, :make, :model, :color, :vin, :status
  json.url inventory_url(inventory, format: :json)
end
