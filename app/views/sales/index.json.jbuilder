json.array!(@sales) do |sale|
  json.extract! sale, :id, :sales_id, :salesperson, :customer, :quote, :year, :model, :color, :vin, :status
  json.url sale_url(sale, format: :json)
end
