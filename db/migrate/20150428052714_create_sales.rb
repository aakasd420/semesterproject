class CreateSales < ActiveRecord::Migration
  def change
    create_table :sales do |t|
      t.integer :sales_id
      t.string :salesperson
      t.string :customer
      t.integer :quote
      t.integer :year
      t.string :model
      t.string :color
      t.string :vin
      t.string :status

      t.timestamps null: false
    end
  end
end
