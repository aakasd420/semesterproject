class CreateFinances < ActiveRecord::Migration
  def change
    create_table :finances do |t|
      t.integer :simple_annual_interest
      t.integer :monthly_payments
      t.integer :amortized

      t.timestamps null: false
    end
  end
end
