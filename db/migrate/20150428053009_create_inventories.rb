class CreateInventories < ActiveRecord::Migration
  def change
    create_table :inventories do |t|
      t.integer :year
      t.string :make
      t.string :model
      t.string :color
      t.string :vin
      t.string :status

      t.timestamps null: false
    end
  end
end
