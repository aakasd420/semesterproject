class CreateDealerships < ActiveRecord::Migration
  def change
    create_table :dealerships do |t|
      t.integer :total_gross_revenue
      t.integer :net_profit
      t.integer :sales_tax_totals

      t.timestamps null: false
    end
  end
end
