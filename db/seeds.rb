# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


Inventory.create(Year: 2007, Make: 'Honda', Model: 'Accord', Color: 'Black', VIN: 'f9h238sh43dne83j', Status: 'Sold')
Inventory.create(Year: 2009, Make: 'Ford', Model: 'Focus', Color: 'Blue', VIN: 'f1h2kjh343kje83j', Status: 'Pending')
Inventory.create(Year: 2012, Make: 'Mazda', Model: 'Accord', Color: 'red', VIN: 'fh238sh43dne83j', Status: 'Sold')
Inventory.create(Year: 2004, Make: 'Toyota', Model: 'Accord', Color: 'Black', VIN: 'fh238sh43dne83j', Status: 'Sold')
Inventory.create(Year: 2008, Make: 'Kia', Model: 'Accord', Color: 'yellow', VIN: 'fh238sh43dne83j', Status: 'Sold')
Inventory.create(Year: 2007, Make: 'Lexus', Model: 'Accord', Color: 'Black', VIN: 'fh238sh43dne83j', Status: 'Sold')