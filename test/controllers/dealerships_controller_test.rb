require 'test_helper'

class DealershipsControllerTest < ActionController::TestCase
  setup do
    @dealership = dealerships(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:dealerships)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create dealership" do
    assert_difference('Dealership.count') do
      post :create, dealership: { net_profit: @dealership.net_profit, sales_tax_totals: @dealership.sales_tax_totals, total_gross_revenue: @dealership.total_gross_revenue }
    end

    assert_redirected_to dealership_path(assigns(:dealership))
  end

  test "should show dealership" do
    get :show, id: @dealership
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @dealership
    assert_response :success
  end

  test "should update dealership" do
    patch :update, id: @dealership, dealership: { net_profit: @dealership.net_profit, sales_tax_totals: @dealership.sales_tax_totals, total_gross_revenue: @dealership.total_gross_revenue }
    assert_redirected_to dealership_path(assigns(:dealership))
  end

  test "should destroy dealership" do
    assert_difference('Dealership.count', -1) do
      delete :destroy, id: @dealership
    end

    assert_redirected_to dealerships_path
  end
end
